class _AnimusItem_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        char[0x40]	m_strModel;		 // this+0x44
        int	m_nIconIDX;		 // this+0x84
        char[0x40]	m_strCivil;		 // this+0x88
        char[0x40]	m_strName;		 // this+0xC8
        int	m_nAnimusClass;		 // this+0x108
        int	m_nAnimusType;		 // this+0x10C
        int	m_nKindClt;		 // this+0x110
        int	m_nFixPart;		 // this+0x114
        int	m_nMoney;		 // this+0x118
        int	m_nStdPrice;		 // this+0x11C
        int	m_nStdPoint;		 // this+0x120
        int	m_nGoldPoint;		 // this+0x124
        int	m_nKillPoint;		 // this+0x128
        int	m_nProcPoint;		 // this+0x12C
        int	m_nStoragePrice;		 // this+0x130
        int	m_bSell;		 // this+0x134
        int	m_bExchange;		 // this+0x138
        int	m_bGround;		 // this+0x13C
        int	m_bStoragePossible;		 // this+0x140
        int	m_bUseableNormalAcc;		 // this+0x144
        char[0x40]	m_strTooltipIndex;		 // this+0x148
        
};
