class _skill_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        int	m_nClass;		 // this+0x44
        int	m_nIconIDX;		 // this+0x48
        int	m_nMastIndex;		 // this+0x4C
        char[0x40]	m_strMastKorName;		 // this+0x50
        char[0x40]	m_strMastEngName;		 // this+0x90
        char[0x40]	m_strKorName;		 // this+0xD0
        char[0x40]	m_strEngName;		 // this+0x110
        int	m_nLv;		 // this+0x150
        int	m_bActivate;		 // this+0x154
        int	m_bEnable;		 // this+0x158
        char[0x40]	m_strUsableRace;		 // this+0x15C
        char[0x40]	m_strActableDst;		 // this+0x19C
        char[0x40]	m_strGradeLimit;		 // this+0x1DC
        int	m_nNeedMastIndex;		 // this+0x21C
        char[0x40]	m_strFixWeapon;		 // this+0x220
        int	m_bFixshield;		 // this+0x260
        int	m_nSpecialType;		 // this+0x264
        int	m_nNeedSpecialType;		 // this+0x268
        int	m_nNeedHP;		 // this+0x26C
        int	m_nNeedFP;		 // this+0x270
        int	m_nNeedSP;		 // this+0x274
        struct _consume_item_list[0x3]	m_ConsumeItemList;		 // this+0x278
        float	m_fActDelay;		 // this+0x29C
        int	m_bCumulType;		 // this+0x2A0
        int	m_nCumulCounter;		 // this+0x2A4
        int	m_nNewEffCount;		 // this+0x2A8
        char[0x40]	m_strEffectCode;		 // this+0x2AC

enum   <unnamed-tag> int
{
          int	attack_type_none;		 // constant 0x0
          int	attack_type_std;		 // constant 0x1
          int	attack_type_stun;		 // constant 0x2

};
        int	m_nAttackable;		 // this+0x2EC
        int[0x7]	m_nAttType;		 // this+0x2F0
        int[0x7]	m_nAttConstant;		 // this+0x30C
        float	m_fAttFormulaConstant;		 // this+0x328
        int	m_nAttNeedBt;		 // this+0x32C
        int	m_nBonusDistance;		 // this+0x330
        char[0x40]	m_strRangeEffCode;		 // this+0x334
        int	m_nTempEffectType;		 // this+0x374
        int	m_nTempParamCode;		 // this+0x378
        float[0x7]	m_fTempValue;		 // this+0x37C
        int	m_nContEffectType;		 // this+0x398
        int	m_nEffLimType;		 // this+0x39C
        int	m_nEffLimType2;		 // this+0x3A0
        int	m_nContAreaType;		 // this+0x3A4
        struct _cont_param_list[0x5]	m_ContParamList;		 // this+0x3A8
        int[0x7]	m_nContEffectSec;		 // this+0x45C
        int	m_nEtc;		 // this+0x478
        float	m_f1_2speed;		 // this+0x47C
        float	m_f1_2distance;		 // this+0x480
        float	m_f2_3speed;		 // this+0x484
        float	m_f2_3distance;		 // this+0x488
        int	m_nEffectClass;		 // this+0x48C
        
};
