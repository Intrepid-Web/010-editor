class _SiegeKitItem_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        int	m_bExist;		 // this+0x44
        char[0x40]	m_strModel;		 // this+0x48
        int	m_nIconIDX;		 // this+0x88
        char[0x40]	m_strName;		 // this+0x8C
        int	m_nKindClt;		 // this+0xCC
        int	m_nItemGrade;		 // this+0xD0
        int	m_nUsableTyoe;		 // this+0xD4
        int	m_nSubType;		 // this+0xD8
        char[0x40]	m_strCivil;		 // this+0xDC
        int	m_nLevelLim;		 // this+0x11C
        int	m_nUpLevelLim;		 // this+0x120
        int	m_nExpertID1;		 // this+0x124
        int	m_nExpertLim1;		 // this+0x128
        int	m_nExpertID2;		 // this+0x12C
        int	m_nExpertLim2;		 // this+0x130
        int	m_nMoney;		 // this+0x134
        int	m_nStdPrice;		 // this+0x138
        int	m_nStdPoint;		 // this+0x13C
        int	m_nStoragePrice;		 // this+0x140
        int	m_bAbr;		 // this+0x144
        int	m_nDurUnit;		 // this+0x148
        int	m_bRepair;		 // this+0x14C
        int	m_nRepPrice;		 // this+0x150
        int	m_nEffState;		 // this+0x154
        float	m_fMinDst;		 // this+0x158
        float	m_fMaxDst;		 // this+0x15C
        int	m_nGACorSpd;		 // this+0x160
        float	m_fGAAF;		 // this+0x164
        int	m_nEff1Code;		 // this+0x168
        float	m_fEff1Unit;		 // this+0x16C
        int	m_nEff2Code;		 // this+0x170
        float	m_fEff2Unit;		 // this+0x174
        int	m_nEff3Code;		 // this+0x178
        float	m_fEff3Unit;		 // this+0x17C
        int	m_nEff4Code;		 // this+0x180
        float	m_fEff4Unit;		 // this+0x184
        int	m_nDuration;		 // this+0x188
        int	m_bSell;		 // this+0x18C
        int	m_bExchange;		 // this+0x190
        int	m_bGround;		 // this+0x194
        int	m_bStoragePossible;		 // this+0x198
        int	m_bUseableNormalAcc;		 // this+0x19C
        int	m_nUpgrade;		 // this+0x1A0
        char[0x40]	m_strTooltipIndex;		 // this+0x1A4
        int	m_nAttEffType;		 // this+0x1E4
        int	m_bIsTime;		 // this+0x1E8
        
};
