class _UnitPart_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        char[0x40]	m_strModle;		 // this+0x44
        int	m_nIconIDX;		 // this+0x84
        char[0x40]	m_strName;		 // this+0x88
        int	m_nFixPart;		 // this+0xC8
        char[0x40]	m_strDefFrame;		 // this+0xCC
        int	m_nWPType;		 // this+0x10C
        int	m_nEffectGroup;		 // this+0x110
        int	m_nNeedBt;		 // this+0x114
        int	m_bAbr;		 // this+0x118
        int	m_nDurUnit;		 // this+0x11C
        int	m_nLevelLim;		 // this+0x120
        int	m_nUpLevelLim;		 // this+0x124
        int	m_nExpertID1;		 // this+0x128
        int	m_nExpertLim1;		 // this+0x12C
        int	m_nExpertID2;		 // this+0x130
        int	m_nExpertLim2;		 // this+0x134
        float	m_fAttGap;		 // this+0x138
        int	m_nAttack_DP;		 // this+0x13C
        float	m_fAttackRange;		 // this+0x140
        int	m_nAttackDel;		 // this+0x144
        float	m_fMoveSpdRev;		 // this+0x148
        int	m_nGAMinAF;		 // this+0x14C
        int	m_nGAMaxAF;		 // this+0x150
        int	m_nAttMastery;		 // this+0x154
        int	m_nGAMinSelProb;		 // this+0x158
        int	m_nGAMaxSelProb;		 // this+0x15C
        int	m_nDefFc;		 // this+0x160
        int	m_nDefMastery;		 // this+0x164
        int	m_nProperty;		 // this+0x168
        int	m_nFireTol;		 // this+0x16C
        int	m_nWaterTol;		 // this+0x170
        int	m_nSoilTol;		 // this+0x174
        int	m_nWindTol;		 // this+0x178
        int	m_nMoney;		 // this+0x17C
        int	m_nStdPrice;		 // this+0x180
        int	m_nStdPoint;		 // this+0x184
        int	m_nRepPrice;		 // this+0x188
        int	m_nDesrepPrice;		 // this+0x18C
        int	m_nBstCha;		 // this+0x190
        float	m_fBstSpd;		 // this+0x194
        int	m_nBackSlt;		 // this+0x198
        int	m_nEff1Code;		 // this+0x19C
        float	m_fEff1Unit;		 // this+0x1A0
        int	m_nEff2Code;		 // this+0x1A4
        float	m_fEff2Unit;		 // this+0x1A8
        int	m_nEff3Code;		 // this+0x1AC
        float	m_fEff3Unit;		 // this+0x1B0
        int	m_nEff4Code;		 // this+0x1B4
        float	m_fEff4Unit;		 // this+0x1B8
        char[0x40]	m_strTooltipIndex;		 // this+0x1BC
        int	m_nAttEffType;		 // this+0x1FC
        int	m_nDefEffType;		 // this+0x200
        
};
